package com.demoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Ejercicio1Application {

	public static void main(String[] args) {
	 ConfigurableApplicationContext context = SpringApplication.run(Ejercicio1Application.class, args);
	 
	 mensaje obj=context.getBean(mensaje.class);
	 obj.Mensaje();
	 
	}

}
