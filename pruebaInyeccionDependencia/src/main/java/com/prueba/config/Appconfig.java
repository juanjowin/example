package com.prueba.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.prueba.services.Persona;
import com.prueba.services.PersonaImpl;

@Configuration
public class Appconfig {
	
	@Bean
	public Persona persona() {
		return new PersonaImpl();
	}

}
