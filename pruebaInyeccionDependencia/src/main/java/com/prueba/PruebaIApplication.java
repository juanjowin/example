package com.prueba;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import com.prueba.services.Persona;


@SpringBootApplication
public class PruebaIApplication implements CommandLineRunner {
	
	
	@Autowired
	private Persona persona;
	

	
	public static void main(String[] args) {
		 SpringApplication.run(PruebaIApplication.class, args);
		
	}
	 @Override
	    public void run(String... args) {
	
	    System.out.println(persona.mostrar()); 
	    }

}
